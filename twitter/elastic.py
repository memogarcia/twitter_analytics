from elasticsearch_dsl import DocType, String, Date, Integer, GeoPoint, GeoShape, Boolean
from elasticsearch_dsl.connections import connections

from utils import get_elasticsearch_hosts


connections.create_connection(hosts=get_elasticsearch_hosts())


class TwitStructure(DocType):
    author_id = String(index='not_analyzed')
    author_screen_name = String(index='not_analyzed')
    author_name = String(index='not_analyzed')
    author_geo_enabled = Boolean()
    author_friends_count = Integer()
    author_followers_count = Integer()
    author_location = String(index='not_analyzed')
    author_verified = Boolean()
    author_created = Date()
    twit_id = String(index='not_analyzed')
    date = Date()
    original_message = String(index='not_analyzed')
    favorite_count = Integer()
    coordinates = GeoPoint(index='not_analyzed')
    geo = GeoShape()
    polarity = String(index='not_analyzed')
    subjectivity = String(index='not_analyzed')
    users_mentioned = String(index='not_analyzed')
    links = String(index='not_analyzed')
    hashtags = String(index='not_analyzed')
    is_retwit = String(index='not_analyzed')
    source = String(index='not_analyzed')

    class Meta:
        index = 'twitter'


if __name__ == '__main__':
    TwitStructure.init()

