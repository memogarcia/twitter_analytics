"""
Analyse twitter data
"""

import json

from textblob import TextBlob
from textblob.exceptions import NotTranslated, TranslatorError

import regex
from elastic import TwitStructure

from pprint import pprint


class Twit(object):
    def __init__(self, twit):
        self.twit = twit
        self.blob = TextBlob(twit.text)

    def detect_language(self):
        return self.blob.detect_language()

    def translate(self, from_lang='es', to='en'):
        try:
            return self.blob.translate(from_lang=from_lang, to=to)
        except (NotTranslated, TranslatorError):
            return self.twit.text

    def re_blob(self, translated_text):
        self.blob = TextBlob(translated_text)

    def to_dict(self):
        return {
            "author_id": self.twit.author.id,
            "author_screen_name": self.twit.author.screen_name,
            # "author_name": self.twit.author.name,
            #"author_geo_enabled": self.twit.author.geo_enabled,
            #"author_friends_count": self.twit.author.friends_count,
            #"author_followers_count": self.twit.author.followers_count,
            #"author_location": self.twit.author.location,
            #"author_verified": self.twit.author.verified,
            #"author_created": self.twit.author.created_at,
            "twit_id": self.twit.id,
            "date": self.twit.created_at,
            "original_message": self.twit.text,
            #"translated_message": "",
            "favorite_count": self.twit.favorite_count,
            "coordinates": self.twit.coordinates,
            "geo": self.twit.geo,
            "place": self.twit.place,
            # "language": self.twit.lang,
            "polarity": self.blob.sentiment.polarity,
            "subjectivity": self.blob.sentiment.subjectivity,
            "users_mentioned": regex.get_usernames_in_mention(self.twit.text),
            "links": regex.get_links(self.twit.text),
            "hashtags": regex.get_hashtags(self.twit.text),
            "is_retwit": regex.is_retwit(self.twit.text),
            #"source": self.twit.source
        }

    @staticmethod
    def to_json(d):
        return json.loads(d)

    def analyse(self, to_json=False):
        language = self.detect_language()
        if language != 'en':
            translated_text = self.translate(from_lang=language, to='en')
            # self.re_blob(translated_text.raw)

        d = self.to_dict()
        self.save(d)

        if to_json:
            return self.to_json(d)
        else:
            return d

    @staticmethod
    def save(d):
        t = TwitStructure(**d)
        t.save()
