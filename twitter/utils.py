import os


def get_filters():
    filters = os.environ.get('TWITTER_FILTERS', '')
    return filters.split(',')


def get_elasticsearch_hosts():
    hosts = os.environ.get('ELASTICSEARCH_HOSTS', '')
    return hosts.split(',')


if __name__ == '__main__':
    print(get_filters())
