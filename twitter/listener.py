"""
A listener and account setting for twitter
"""

import os

import tweepy

from analytics import Twit
from utils import get_filters


class Listener(tweepy.StreamListener):
    def on_status(self, status):
        twit = Twit(status)
        twit.analyse()


class Crawler(object):
    def connect(self):
        consumer_key = os.environ.get('TWITTER_CONSUMER_KEY')
        consumer_secret = os.environ.get('TWITTER_CONSUMER_SECRET')
        access_key = os.environ.get('TWITTER_ACCESS_KEY')
        secret_key = os.environ.get('TWITTER_SECRET_KEY')

        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_key, secret_key)

        return auth

    def crawl(self):
        filters = get_filters()
        auth = self.connect()
        streamer = tweepy.Stream(auth=auth,
                                 listener=Listener())
        streamer.filter(track=filters, async=True)


if __name__ == '__main__':
    crawler = Crawler()
    crawler.crawl()
