"""
define common regular expressions to be used by fork-bd
"""

import re


LINK = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
USERNAME = '(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9]+)'
HASHTAG = '#(\w+)'


def get_links(text):
    return re.findall(LINK, text)


def get_usernames_in_mention(text):
    return re.findall(USERNAME, text)


def get_hashtags(text):
    return re.findall(HASHTAG, text)


def is_retwit(text):
    if text.startswith('RT'):
        return True
    return False
